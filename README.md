# xscreensaver.service
systemd unit file for xscreensaver

Copy "xscreensaver.service" to /lib/systemd/system/ and run `systemctl daemon-reload`.

This unit file was configured for lightdm. If not using lightdm, change the "Requisite" and "User" lines to the appropriate value for your display manager.
